import 'package:flutter/material.dart';
import 'package:notes/screens/about_screen.dart';
import 'package:notes/screens/add_category_screen.dart';
import 'package:notes/screens/add_note_screen.dart';
import 'package:notes/screens/categories_screen.dart';
import 'package:notes/screens/category_name_screen.dart';
import 'package:notes/screens/edite_note_screen.dart';
import 'package:notes/screens/profile_screen.dart';
import 'package:notes/screens/settings_screen.dart';
import 'package:notes/screens/sign_in_screen.dart';
import 'package:notes/screens/sign_up_screen.dart';
import 'screens/launch_screen.dart';

main() {
  runApp(MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        appBarTheme: AppBarTheme(
          backgroundColor: Colors.transparent,
          centerTitle: true,
          elevation: 0,
          titleTextStyle: TextStyle(
            fontSize: 20,
            color: Colors.black,
            fontWeight: FontWeight.bold,
            fontFamily: 'nunito',
          ),
          iconTheme: IconThemeData(
            color: Colors.black,
            size: 24,
          ),
        ),
      ),
      routes: {
        '/launch_screen': (context) => LaunchScreen(),
        '/login_screen': (context) => SignInScreen(),
        '/signup_screen': (context) => SignUpScreen(),
        '/category_screen': (context) => CategoriesScreen(),
        '/add_category_screen': (context) => AddCategoryScreen(),
        '/add_note_screen': (context) => AddNoteScreen(),
        '/about_screen': (context) => AboutScreen(),
        '/settings_screen': (context) => SettingsScreen(),
        '/profile_screen': (context) => ProfileScreen(),
        '/edit_note_screen': (context) => EditNoteScreen(),
        '/category_name_screen': (context) => CategoryNameScreen(),
      },
      initialRoute: '/launch_screen',
    );
  }
}
