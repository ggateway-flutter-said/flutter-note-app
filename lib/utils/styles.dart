
import 'package:flutter/material.dart';

class AppStyleColor{

  static Color TEXT_COLOR_GRAY = Color(0xFF707070);
  static Color TEXT_COLOR_WHITE = Color(0xFFFFFFFF);
  static Color BUTTON_COLOR = Color(0xFF6A90F2);


}