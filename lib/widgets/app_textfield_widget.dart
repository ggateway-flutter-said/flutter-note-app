import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:notes/utils/size_config.dart';

class AppTextFieldWidget extends StatelessWidget {
  final String hint;
  final TextEditingController controller;
  final TextInputType textInputType;
  final bool isPassword;
  final String? errorMsg;
  final double hintSize;
  final double textSize;

  AppTextFieldWidget({
    required this.hint,
    required this.controller,
    required this.textSize,
    required this.hintSize,
    this.textInputType = TextInputType.text,
    this.isPassword = false,
    this.errorMsg,
  });

  @override
  Widget build(BuildContext context) {
    return TextField(
      obscureText: isPassword,
      keyboardType: textInputType,
      cursorColor: Colors.black,
      cursorHeight: SizeConfig.scaleHeight(25),
      decoration: InputDecoration(
        hintText: hint,
        errorText: errorMsg,
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.grey.shade500,
            width: 1,
          ),
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.grey.shade300,
            width: SizeConfig.scaleWidth(1),
          ),
        ),
        errorBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.red,
            width: SizeConfig.scaleWidth(1),
          ),
        ),
        focusedErrorBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.red,
            width: SizeConfig.scaleWidth(1),
          ),
        ),
        hintStyle: TextStyle(
          fontFamily: 'Roboto',
          fontSize: hintSize,
          fontWeight: FontWeight.w300,
        ),
      ),
      style: TextStyle(
        fontFamily: 'Roboto',
        fontSize: textSize,
      ),
      controller: controller,
    );
  }
}
