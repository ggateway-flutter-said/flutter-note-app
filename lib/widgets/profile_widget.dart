import 'package:flutter/material.dart';
import 'package:notes/utils/size_config.dart';
import 'package:notes/utils/styles.dart';

import 'app_text_widget.dart';

class ProfileWidget extends StatelessWidget {
  final String title;
  final String numberNote;

  ProfileWidget({
    required this.title,
    required this.numberNote,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.scaleHeight(58),
      width: SizeConfig.scaleWidth(85),
      decoration: BoxDecoration(
        color: Colors.transparent,
        border: Border.all(color: AppStyleColor.BUTTON_COLOR, width: SizeConfig.scaleWidth(1)),
        borderRadius: BorderRadius.all(
          Radius.circular(
            SizeConfig.scaleHeight(10),
          ),
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          AppTextWidget(
            text: title,
            fontWeight: FontWeight.w500,
            fontSize: SizeConfig.scaleTextFont(12),
            color: AppStyleColor.BUTTON_COLOR,
          ),
          AppTextWidget(
            text: numberNote,
            fontWeight: FontWeight.w500,
            fontSize: SizeConfig.scaleTextFont(12),
            color: AppStyleColor.TEXT_COLOR_GRAY,
          ),
        ],
      ),
    );
  }
}
