import 'package:flutter/material.dart';
import 'package:notes/utils/size_config.dart';
import 'package:notes/utils/styles.dart';
import 'package:notes/widgets/app_text_widget.dart';

class SettingsWidget extends StatelessWidget {
  final bool isLeft;
  final IconData icon;
  final String title;
  final String subTitle;
  void Function()? onPressed;

  SettingsWidget(
      {required this.isLeft,
      required this.icon,
      required this.title,
      required this.subTitle,
      this.onPressed});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed ?? () {},
      child: Container(
        clipBehavior: Clip.antiAlias,
        height: SizeConfig.scaleHeight(70),
        margin: EdgeInsetsDirectional.only(
          top: SizeConfig.scaleHeight(10),
          start: SizeConfig.scaleWidth(18),
          end: SizeConfig.scaleWidth(18),
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.black.withAlpha(25),
              offset: Offset(0, 0),
              blurRadius: 6,
              spreadRadius: 3,
            )
          ],
          borderRadius: BorderRadius.all(
            Radius.circular(SizeConfig.scaleHeight(7)),
          ),
        ),
        child: Row(
          children: [
            Container(
              color: isLeft ? AppStyleColor.BUTTON_COLOR : Colors.white,
              width: SizeConfig.scaleWidth(5),
            ),
            Padding(
              padding: EdgeInsetsDirectional.only(
                start: SizeConfig.scaleWidth(15),
                end: SizeConfig.scaleWidth(15),
                top: SizeConfig.scaleHeight(10),
                bottom: SizeConfig.scaleHeight(10),
              ),
              child: CircleAvatar(
                radius: SizeConfig.scaleHeight(30),
                backgroundColor: AppStyleColor.BUTTON_COLOR,
                child: Icon(
                  icon,
                  size: SizeConfig.scaleHeight(25),
                  color: Colors.white,
                ),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AppTextWidget(
                  text: title,
                  fontWeight: FontWeight.w500,
                  fontSize: SizeConfig.scaleTextFont(13),
                  fontFamily: 'quicksand',
                ),
                AppTextWidget(
                  text: subTitle,
                  fontWeight: FontWeight.w500,
                  fontSize: SizeConfig.scaleTextFont(12),
                  fontFamily: 'quicksand',
                  color: Colors.grey,
                ),
              ],
            ),
            Spacer(),
            IconButton(
                icon: Icon(
                  Icons.arrow_forward_ios,
                  size: SizeConfig.scaleHeight(18),
                  color: Colors.grey,
                ),
                onPressed: () {}),
            Container(
              color: isLeft ? Colors.white : AppStyleColor.BUTTON_COLOR,
              width: SizeConfig.scaleHeight(5),
            ),
          ],
        ),
      ),
    );
  }
}
