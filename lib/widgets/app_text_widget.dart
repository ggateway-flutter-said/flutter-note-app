import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:notes/utils/size_config.dart';

class AppTextWidget extends StatelessWidget {
  final String text;
  final String fontFamily;
  final double fontSize;
  final FontWeight fontWeight;
  final TextAlign textAlign;
  final Color color;


  AppTextWidget({
    required this.text,
    this.fontFamily ='Roboto',
    this.fontSize = 16,
    this.fontWeight = FontWeight.normal,
    this.textAlign = TextAlign.start,
    this.color = Colors.black,

  });

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontSize: fontSize,
        fontFamily: fontFamily,
        fontWeight: fontWeight,
        color: color
      ),
      textAlign: textAlign,
    ) ;
  }
}
