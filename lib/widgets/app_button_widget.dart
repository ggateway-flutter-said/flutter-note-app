import 'package:flutter/material.dart';
import 'package:notes/utils/styles.dart';
import 'package:notes/widgets/app_text_widget.dart';

class AppButtonWidget extends StatelessWidget {
  final void Function()? onPressed;
  final String label;

  AppButtonWidget(this.onPressed, this.label);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      child: AppTextWidget(
        color: AppStyleColor.TEXT_COLOR_WHITE,
        text: label,
        fontSize: 22,
        fontWeight: FontWeight.w500,
      ),

      style: ElevatedButton.styleFrom(
        primary: AppStyleColor.BUTTON_COLOR,
        minimumSize: Size(double.infinity,57),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15))
        )
      ),
    );
  }
}
