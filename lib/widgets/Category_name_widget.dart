import 'package:flutter/material.dart';
import 'package:notes/utils/size_config.dart';
import 'package:notes/utils/styles.dart';
import 'package:notes/widgets/app_text_widget.dart';

class CategoryNameWidget extends StatelessWidget {
  String noteTitle;
  String noteDescription;
  bool iconChecked;

  CategoryNameWidget({
    required this.noteTitle,
    required this.noteDescription,
    this.iconChecked = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(
        horizontal: SizeConfig.scaleWidth(18),
        vertical: SizeConfig.scaleHeight(10),
      ),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            offset: Offset(0,2),
            color: Colors.black.withOpacity(0.16),
            blurRadius: 6,
            spreadRadius: 3
          )
        ],
        color: AppStyleColor.BUTTON_COLOR,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Container(
        width: double.infinity,
        margin: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(5)),
        color: Colors.white,
        alignment: AlignmentDirectional.center,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: SizeConfig.scaleHeight(25)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  AppTextWidget(
                    text: noteTitle,
                    fontSize: SizeConfig.scaleTextFont(13),
                    fontFamily: 'quicksand',
                    color: Colors.black,
                    fontWeight: FontWeight.w500,
                  ),
                  SizedBox(
                    height: SizeConfig.scaleHeight(5),
                  ),
                  SizedBox(
                    child: AppTextWidget(
                      text: noteDescription,
                      fontSize: SizeConfig.scaleTextFont(12),
                      fontFamily: 'quicksand',
                      color: Colors.grey,
                      fontWeight: FontWeight.w500,
                    ),
                    width: SizeConfig.scaleWidth(240),
                  ),
                ],
              ),
              Icon(
                Icons.check_circle,
                size: SizeConfig.scaleHeight(28),
                color: iconChecked ? Colors.green : Colors.grey,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
