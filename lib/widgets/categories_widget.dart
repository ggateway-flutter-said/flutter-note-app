import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:notes/utils/size_config.dart';
import 'package:notes/utils/styles.dart';
import 'package:notes/widgets/app_text_widget.dart';

class CategoriesWidget extends StatelessWidget {
  final String title;
  final String subTitle;

  CategoriesWidget({
    required this.title,
    required this.subTitle,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, '/category_name_screen');
      },
      child: Container(
        clipBehavior: Clip.antiAlias,
        margin: EdgeInsetsDirectional.only(
            top: SizeConfig.scaleHeight(10),
            end: SizeConfig.scaleWidth(18),
            start: SizeConfig.scaleWidth(18)),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.black.withAlpha(25),
              offset: Offset(0, 0),
              blurRadius: 6,
              spreadRadius: 3,
            )
          ],
          borderRadius: BorderRadius.all(
            Radius.circular(SizeConfig.scaleWidth(7)),
          ),
        ),
        height: SizeConfig.scaleHeight(70),
        child: Row(
          children: [
            Padding(
              padding: EdgeInsetsDirectional.only(
                start: SizeConfig.scaleWidth(15),
                end: SizeConfig.scaleWidth(15),
                top: SizeConfig.scaleHeight(10),
                bottom: SizeConfig.scaleHeight(10),
              ),
              child: CircleAvatar(
                radius: SizeConfig.scaleWidth(30),
                child: AppTextWidget(
                  text: 'W',
                  color: Colors.white,
                  fontFamily: 'quicksand',
                  fontSize: SizeConfig.scaleTextFont(22),
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AppTextWidget(
                  text: title,
                  fontWeight: FontWeight.w500,
                  fontSize: 13,
                  fontFamily: 'quicksand',
                ),
                AppTextWidget(
                  text: subTitle,
                  fontWeight: FontWeight.w500,
                  fontSize: 12,
                  fontFamily: 'quicksand',
                  color: AppStyleColor.TEXT_COLOR_GRAY,
                ),
              ],
            ),
            Spacer(),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.scaleWidth(10),
                  vertical: SizeConfig.scaleHeight(10)),
              child: Icon(
                Icons.delete,
                color: Colors.red,
                size: SizeConfig.scaleHeight(15),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.scaleWidth(2),
                  vertical: SizeConfig.scaleHeight(2)),
              alignment: AlignmentDirectional.center,
              color: Colors.blue,
              width: SizeConfig.scaleHeight(20),
              height: SizeConfig.scaleHeight(70),
              child: IconButton(
                padding: EdgeInsets.zero,
                icon: Icon(
                  Icons.edit,
                  color: Colors.white,
                  size: SizeConfig.scaleHeight(10),
                ),
                onPressed: () =>
                    Navigator.pushNamed(context, '/edit_note_screen'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
