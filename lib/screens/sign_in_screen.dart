import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:notes/helpers/helper.dart';
import 'package:notes/utils/size_config.dart';
import 'package:notes/utils/styles.dart';
import 'package:notes/widgets/app_textfield_widget.dart';
import 'package:notes/widgets/app_button_widget.dart';
import 'package:notes/widgets/app_text_widget.dart';

class SignInScreen extends StatefulWidget {
  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  late TapGestureRecognizer _tapGestureRecognizer;
  late TextEditingController _usernameController;
  late TextEditingController _passwordController;
  String? errorUserName;
  String? errorPassword;

  @override
  void initState() {
    super.initState();

    _tapGestureRecognizer = TapGestureRecognizer();
    _tapGestureRecognizer.onTap = onTapSingInText;
    _usernameController = TextEditingController(text: 'Kendrick');
    _passwordController = TextEditingController();
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  void onTapSingInText() {
    Navigator.pushNamed(context, '/signup_screen');
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/app_background.png'),
          fit: BoxFit.cover,
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsetsDirectional.only(
              top: SizeConfig.scaleHeight(106),
              start: SizeConfig.scaleWidth(25),
              end: SizeConfig.scaleWidth(25),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AppTextWidget(
                  text: 'Sign In',
                  fontSize: SizeConfig.scaleTextFont(30),
                  fontFamily: 'nunito',
                ),
                AppTextWidget(
                  text: 'Login to start using app',
                  fontWeight: FontWeight.w300,
                  color: AppStyleColor.TEXT_COLOR_GRAY,
                  fontSize: SizeConfig.scaleTextFont(18),
                ),
                Card(
                  margin: EdgeInsetsDirectional.only(
                      top: SizeConfig.scaleHeight(81),
                      bottom: SizeConfig.scaleHeight(30)),
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: SizeConfig.scaleWidth(20),
                        vertical: SizeConfig.scaleHeight(30)),
                    child: Column(
                      children: [
                        AppTextFieldWidget(
                          hintSize: SizeConfig.scaleTextFont(22),
                          textSize: SizeConfig.scaleTextFont(22),
                          controller: _usernameController,
                          hint: 'UserName',
                          errorMsg: errorUserName,
                        ),
                        SizedBox(
                          height: SizeConfig.scaleHeight(30),
                        ),
                        AppTextFieldWidget(
                          hintSize: SizeConfig.scaleTextFont(22),
                          textSize: SizeConfig.scaleTextFont(22),
                          controller: _passwordController,
                          hint: 'Password',
                          isPassword: true,
                          errorMsg: errorPassword,
                        ),
                      ],
                    ),
                  ),
                ),
                AppButtonWidget(
                  () => performLogin(),
                  'Login',
                ),
                SizedBox(
                  height: SizeConfig.scaleHeight(10),
                ),
                Container(
                  width: double.infinity,
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: 'Don’t have an account?',
                      style: TextStyle(
                        fontSize: SizeConfig.scaleTextFont(18),
                        fontFamily: 'Roboto',
                        fontWeight: FontWeight.w300,
                        color: AppStyleColor.TEXT_COLOR_GRAY,
                      ),
                      children: [
                        TextSpan(
                          text: ' Sing in',
                          style: TextStyle(
                            fontSize: SizeConfig.scaleTextFont(18),
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.w500,
                            color: Colors.black,
                          ),
                          recognizer: _tapGestureRecognizer,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      // body: ,
    );
  }

  void performLogin() {
    if (checkData()) {
      login();
    }
  }

  bool checkData() {
    if (_usernameController.text.isNotEmpty &&
        _passwordController.text.isNotEmpty) {
      checkErrors();
      return true;
    }
    checkErrors();
    Helper.showSnackBar(context,
        text: 'Please, enter required data', error: true);
    return false;
  }

  void login() {
    Future.delayed(
      Duration(seconds: 3),
      () => Navigator.pushReplacementNamed(context, '/category_screen'),
    );
  }

  void checkErrors() {
    setState(() {
      errorUserName =
          _usernameController.text.isEmpty ? 'Enter email address' : null;
      errorPassword =
          _passwordController.text.isEmpty ? 'Enter password' : null;
    });
  }
}
