import 'package:flutter/material.dart';
import 'package:notes/utils/size_config.dart';
import 'package:notes/utils/styles.dart';
import 'package:notes/widgets/app_text_widget.dart';

class LaunchScreen extends StatefulWidget {
  @override
  _LaunchScreenState createState() => _LaunchScreenState();
}

class _LaunchScreenState extends State<LaunchScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(
      Duration(seconds: 3),
      () => Navigator.pushReplacementNamed(context, '/login_screen'),
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Stack(
        children: [
          Container(
            alignment: AlignmentDirectional.center,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/app_background.png'),
                fit: BoxFit.cover,
              ),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                CircleAvatar(
                  backgroundImage: AssetImage('assets/images/logo.png'),
                  radius: SizeConfig.scaleHeight(60),
                ),
                SizedBox(
                  height: SizeConfig.scaleHeight(12),
                ),
                AppTextWidget(
                    text: 'My Notes',
                    fontSize: SizeConfig.scaleTextFont(30),
                    fontFamily: 'nunito'),
                AppTextWidget(
                  text: 'For Organized Life',
                  fontWeight: FontWeight.w300,
                  color: AppStyleColor.TEXT_COLOR_GRAY,
                  fontSize: SizeConfig.scaleTextFont(15),
                ),
              ],
            ),
          ),
          PositionedDirectional(
            child: AppTextWidget(
              text: 'iOS Course - Notes App V1.0',
              fontWeight: FontWeight.w300,
              color: AppStyleColor.TEXT_COLOR_GRAY,
              fontSize: SizeConfig.scaleTextFont(15),
              textAlign: TextAlign.center,
            ),
            bottom: SizeConfig.scaleHeight(20),
            start: 0,
            end: 0,
          )
        ],
      ),
    );
  }
}
