import 'package:flutter/material.dart';
import 'package:notes/utils/size_config.dart';
import 'package:notes/utils/styles.dart';
import 'package:notes/widgets/app_text_widget.dart';
import 'package:notes/widgets/settings_widget.dart';

class SettingsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Settings',
          style: TextStyle(
              fontSize: SizeConfig.scaleTextFont(22),
              fontFamily: 'quicksand',
              fontWeight: FontWeight.bold,
              color: Colors.black),
        ),
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: Icon(Icons.arrow_back_ios),
        ),
      ),
      body: Stack(
        children: [
          Padding(
            padding: EdgeInsetsDirectional.only(
              top: SizeConfig.scaleHeight(25),
            ),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(SizeConfig.scaleHeight(25)),
                  alignment: Alignment.center,
                  margin: EdgeInsetsDirectional.only(
                    bottom: SizeConfig.scaleHeight(8),
                  ),
                  decoration: BoxDecoration(
                    color: AppStyleColor.BUTTON_COLOR,
                    shape: BoxShape.circle,
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black.withOpacity(0.16),
                          offset: Offset(3, 6),
                          spreadRadius: 2,
                          blurRadius: 6)
                    ],
                  ),
                  child: AppTextWidget(
                    text: 'S',
                    fontSize: SizeConfig.scaleTextFont(22),
                    fontFamily: 'quicksand',
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
                AppTextWidget(
                  text: 'Said Alnahhal',
                  fontSize: SizeConfig.scaleTextFont(15),
                  fontFamily: 'quicksand',
                  fontWeight: FontWeight.w500,
                ),
                AppTextWidget(
                  text: 'dev.said98@gmail.com',
                  fontSize: SizeConfig.scaleTextFont(13),
                  fontWeight: FontWeight.w500,
                  color: Colors.grey,
                  fontFamily: 'quicksand',
                ),
                Divider(
                  color: Colors.grey.shade300,
                  thickness: SizeConfig.scaleHeight(1),
                  endIndent: SizeConfig.scaleWidth(45),
                  indent: SizeConfig.scaleWidth(45),
                  height: SizeConfig.scaleHeight(12),
                ),
                SizedBox(
                  height: SizeConfig.scaleHeight(15),
                ),
                SettingsWidget(
                  isLeft: true,
                  icon: Icons.language_outlined,
                  title: 'Language',
                  subTitle: 'Selected language: EN',
                ),
                SettingsWidget(
                  isLeft: false,
                  icon: Icons.person_outline,
                  title: 'Profile',
                  subTitle: 'Update your data…',
                  onPressed: () =>
                      Navigator.pushNamed(context, '/profile_screen'),
                ),
                SettingsWidget(
                  isLeft: true,
                  icon: Icons.phone_android_outlined,
                  title: 'About App',
                  subTitle: 'What is notes app?',
                  onPressed: () =>
                      Navigator.pushNamed(context, '/about_screen'),
                ),
                SettingsWidget(
                  isLeft: false,
                  icon: Icons.info_outline,
                  title: 'About course',
                  subTitle: 'Describe the course in brief',
                ),
                SettingsWidget(
                  isLeft: true,
                  icon: Icons.power_settings_new_outlined,
                  title: 'Logout',
                  subTitle: 'Waiting your return…',
                  onPressed: () =>
                      Navigator.pushReplacementNamed(context, '/login_screen'),
                ),
              ],
            ),
          ),
          PositionedDirectional(
            child: AppTextWidget(
              text: 'iOS Course - Notes App V1.0',
              fontWeight: FontWeight.w300,
              color: AppStyleColor.TEXT_COLOR_GRAY,
              fontSize: SizeConfig.scaleTextFont(15),
              textAlign: TextAlign.center,
            ),
            bottom: SizeConfig.scaleHeight(20),
            start: 0,
            end: 0,
          ),
        ],
      ),
    );
  }
}
