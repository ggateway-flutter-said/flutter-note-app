import 'package:flutter/material.dart';
import 'package:notes/utils/size_config.dart';
import 'package:notes/widgets/Category_name_widget.dart';

class CategoryNameScreen extends StatelessWidget {
  const CategoryNameScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Category Name',
          style: TextStyle(
              fontSize: SizeConfig.scaleTextFont(22),
              fontFamily: 'quicksand',
              fontWeight: FontWeight.bold,
              color: Colors.black),
        ),
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: Icon(Icons.arrow_back_ios),
        ),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pushNamed(context, '/add_note_screen');
            },
            icon: Icon(Icons.add_circle_outlined),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            CategoryNameWidget(
              noteDescription:
                  'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry/’s standard dummy text ever since the 1500s,noteDescription:',
              noteTitle: 'Note Title',
              iconChecked: true,
            ),
            CategoryNameWidget(
              noteDescription:
                  'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry/’s standard dummy text ever since the 1500s,noteDescription:',
              noteTitle: 'Note Title',
            ),
            CategoryNameWidget(
              noteDescription:
                  'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry/’s standard dummy text ever since the 1500s,noteDescription:',
              noteTitle: 'Note Title',
              iconChecked: true,
            ),
            CategoryNameWidget(
              noteDescription:
                  'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry/’s standard dummy text ever since the 1500s,noteDescription:',
              noteTitle: 'Note Title',
              iconChecked: true,
            ),
            CategoryNameWidget(
              noteDescription:
                  'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry/’s standard dummy text ever since the 1500s,noteDescription:',
              noteTitle: 'Note Title',
            ),
            CategoryNameWidget(
              noteDescription:
                  'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry/’s standard dummy text ever since the 1500s,noteDescription:',
              noteTitle: 'Note Title',
            ),
          ],
        ),
      ),
    );
  }
}
