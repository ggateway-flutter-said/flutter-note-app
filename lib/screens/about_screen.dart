import 'package:flutter/material.dart';
import 'package:notes/utils/size_config.dart';
import 'package:notes/utils/styles.dart';
import 'package:notes/widgets/app_text_widget.dart';

class AboutScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.pop(context),
        ),
        title: AppTextWidget(
            text: 'About',
            fontSize: SizeConfig.scaleTextFont(22),
            fontFamily: 'quicksand',
            fontWeight: FontWeight.bold,
            color: Colors.black),
      ),
      body: Stack(
        children: [
          Container(
            alignment: AlignmentDirectional.center,
            decoration: BoxDecoration(
              image: DecorationImage(
                colorFilter: new ColorFilter.mode(
                  Colors.white.withOpacity(0.3),
                  BlendMode.dstATop,
                ),
                image: AssetImage('assets/images/app_background.png'),
                fit: BoxFit.cover,
              ),
            ),
            child: Container(
              padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.scaleWidth(60),
                  vertical: SizeConfig.scaleHeight(30)),
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(
                  color: Colors.blue,
                  width: SizeConfig.scaleHeight(5),
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(
                    SizeConfig.scaleHeight(10),
                  ),
                ),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  CircleAvatar(
                    backgroundImage: AssetImage('assets/images/logo.png'),
                    radius: SizeConfig.scaleHeight(60),
                  ),
                  AppTextWidget(
                      text: 'My Notes',
                      fontSize: SizeConfig.scaleTextFont(30),
                      fontFamily: 'nunito'),
                  AppTextWidget(
                    text: 'For Organized Life',
                    fontWeight: FontWeight.w300,
                    color: AppStyleColor.TEXT_COLOR_GRAY,
                    fontSize: SizeConfig.scaleTextFont(15),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            child: AppTextWidget(
              text: 'iOS Course - Notes App V1.0',
              fontWeight: FontWeight.w300,
              color: AppStyleColor.TEXT_COLOR_GRAY,
              fontSize: 15,
              textAlign: TextAlign.center,
            ),
            bottom: 20,
            right: 0,
            left: 0,
          )
        ],
      ),
    );
  }
}
