import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:notes/helpers/helper.dart';
import 'package:notes/utils/size_config.dart';
import 'package:notes/utils/styles.dart';
import 'package:notes/widgets/app_textfield_widget.dart';
import 'package:notes/widgets/app_button_widget.dart';
import 'package:notes/widgets/app_text_widget.dart';

class EditNoteScreen extends StatefulWidget {
  const EditNoteScreen({Key? key}) : super(key: key);

  @override
  _EditNoteScreenState createState() => _EditNoteScreenState();
}

class _EditNoteScreenState extends State<EditNoteScreen> {
  late TextEditingController _nameNoteController;
  late TextEditingController _descriptionController;
  String? errorNameNote;
  String? errorDescription;

  @override
  void initState() {
    super.initState();
    _nameNoteController = TextEditingController(text: 'Note Tile');
    _descriptionController = TextEditingController(text: 'Description');
  }

  @override
  void dispose() {
    _nameNoteController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsetsDirectional.only(
            top: SizeConfig.scaleHeight(15),
            start: SizeConfig.scaleWidth(25),
            end: SizeConfig.scaleWidth(25),
          ),
          child: Column(
            children: [
              AppTextWidget(
                text: 'Edit Category',
                fontSize: SizeConfig.scaleTextFont(30),
                fontFamily: 'nunito',
              ),
              AppTextWidget(
                text: 'Edite Category',
                fontWeight: FontWeight.w300,
                color: AppStyleColor.TEXT_COLOR_GRAY,
                fontSize: SizeConfig.scaleTextFont(18),
              ),
              Card(
                margin: EdgeInsetsDirectional.only(
                    top: SizeConfig.scaleHeight(81),
                    bottom: SizeConfig.scaleHeight(30)),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: SizeConfig.scaleWidth(20),
                      vertical: SizeConfig.scaleHeight(30)),
                  child: Column(
                    children: [
                      AppTextFieldWidget(
                        hintSize: SizeConfig.scaleTextFont(22),
                        textSize: SizeConfig.scaleTextFont(22),
                        controller: _nameNoteController,
                        hint: 'Category Title',
                        errorMsg: errorNameNote,
                      ),
                      SizedBox(
                        height: SizeConfig.scaleHeight(30),
                      ),
                      AppTextFieldWidget(
                        hintSize: SizeConfig.scaleTextFont(22),
                        textSize: SizeConfig.scaleTextFont(22),
                        controller: _descriptionController,
                        hint: 'Category Description',
                        errorMsg: errorDescription,
                      ),
                    ],
                  ),
                ),
              ),
              AppButtonWidget(
                    () => performData(),
                'Save',
              ),
            ],
          ),
        ),
      ),
    );
  }

  void performData() {
    if (checkData()) {
      addNote();
    }
  }

  bool checkData() {
    if (_nameNoteController.text.isNotEmpty &&
        _descriptionController.text.isNotEmpty) {
      checkErrors();
      return true;
    }
    checkErrors();
    Helper.showSnackBar(context,
        text: 'Please, enter required data', error: true);
    return false;
  }

  void addNote() {
    Navigator.pushReplacementNamed(context, '/category_screen');
  }

  void checkErrors() {
    setState(() {
      errorNameNote =
      _nameNoteController.text.isEmpty ? 'Enter Name Category' : null;
      errorDescription =
      _descriptionController.text.isEmpty ? 'Enter Description Category' : null;
    });
  }
}
