import 'package:flutter/material.dart';
import 'package:notes/utils/size_config.dart';
import 'package:notes/widgets/categories_widget.dart';

class CategoriesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Categories',
          style: TextStyle(
              fontSize: SizeConfig.scaleTextFont(22),
              fontFamily: 'quicksand',
              fontWeight: FontWeight.bold,
              color: Colors.black),
        ),
        actions: [
          IconButton(
            onPressed: () => Navigator.pushNamed(context, '/settings_screen'),
            icon: Icon(Icons.settings),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            CategoriesWidget(title: 'Work', subTitle: 'Notes for work...'),
            CategoriesWidget(title: 'Work', subTitle: 'Notes for work...'),
            CategoriesWidget(title: 'Work', subTitle: 'Notes for work...'),
            CategoriesWidget(title: 'Work', subTitle: 'Notes for work...'),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.pushNamed(context, '/add_category_screen'),
        child: Icon(
          Icons.add,
          size: SizeConfig.scaleTextFont(30),
        ),
      ),
    );
  }
}
