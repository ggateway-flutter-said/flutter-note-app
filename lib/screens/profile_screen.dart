import 'package:flutter/material.dart';
import 'package:notes/helpers/helper.dart';
import 'package:notes/utils/size_config.dart';
import 'package:notes/widgets/app_textfield_widget.dart';
import 'package:notes/widgets/app_button_widget.dart';
import 'package:notes/widgets/app_text_widget.dart';
import 'package:notes/widgets/profile_widget.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  late TextEditingController _firstNameController;
  late TextEditingController _lastNameController;
  late TextEditingController _emailController;
  late TextEditingController _phoneController;

  String? _firstNameError;
  String? _lastNameError;
  String? _emailError;
  String? _phoneError;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _firstNameController = TextEditingController(text: 'Said');
    _lastNameController = TextEditingController(text: 'Alnahhal');
    _emailController = TextEditingController(text: 'dev.said98@gmail.com');
    _phoneController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.pop(context),
        ),
        title: AppTextWidget(
            text: 'Profile',
            fontSize: SizeConfig.scaleTextFont(22),
            fontFamily: 'quicksand',
            fontWeight: FontWeight.bold,
            color: Colors.black),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(SizeConfig.scaleHeight(25)),
          child: Column(
            children: [
              Card(
                elevation: 5,
                child: ListTile(
                  leading: CircleAvatar(
                    radius: SizeConfig.scaleHeight(23),
                    child: AppTextWidget(
                      text: 'S',
                      color: Colors.white,
                      fontSize: SizeConfig.scaleTextFont(20),
                    ),
                  ),
                  title: AppTextWidget(
                    text: 'Said alnahhal',
                    fontSize: SizeConfig.scaleTextFont(13),
                    fontFamily: 'quicksand',
                    color: Colors.black,
                    fontWeight: FontWeight.w500,
                  ),
                  subtitle: AppTextWidget(
                    text: 'dev.saeeid98@gmail',
                    fontSize: SizeConfig.scaleTextFont(12),
                    fontFamily: 'quicksand',
                    color: Colors.grey,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              Padding(
                padding:
                    EdgeInsets.symmetric(vertical: SizeConfig.scaleHeight(15)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ProfileWidget(
                      title: 'Categories',
                      numberNote: '14',
                    ),
                    ProfileWidget(
                      title: 'Done Notes',
                      numberNote: '14',
                    ),
                    ProfileWidget(
                      title: 'Waiting Notes',
                      numberNote: '14',
                    ),
                  ],
                ),
              ),
              Column(
                children: [
                  Card(
                    elevation: SizeConfig.scaleHeight(5),
                    margin: EdgeInsetsDirectional.only(
                      // top: SizeConfig.scaleHeight(81),
                      bottom: SizeConfig.scaleHeight(30),
                    ),
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: SizeConfig.scaleWidth(15),
                        vertical: SizeConfig.scaleHeight(15),
                      ),
                      child: Column(
                        children: [
                          AppTextFieldWidget(
                            hint: 'FirstName',
                            controller: _firstNameController,
                            errorMsg: _firstNameError,
                            hintSize: SizeConfig.scaleTextFont(14),
                            textSize: SizeConfig.scaleTextFont(14),
                          ),
                          SizedBox(
                            height: SizeConfig.scaleHeight(18),
                          ),
                          AppTextFieldWidget(
                            hint: 'LastName',
                            controller: _lastNameController,
                            errorMsg: _lastNameError,
                            hintSize: SizeConfig.scaleTextFont(14),
                            textSize: SizeConfig.scaleTextFont(14),
                          ),
                          SizedBox(
                            height: SizeConfig.scaleHeight(18),
                          ),
                          AppTextFieldWidget(
                            hint: 'Email',
                            controller: _emailController,
                            textInputType: TextInputType.emailAddress,
                            errorMsg: _emailError,
                            hintSize: SizeConfig.scaleTextFont(14),
                            textSize: SizeConfig.scaleTextFont(14),
                          ),
                          SizedBox(
                            height: SizeConfig.scaleHeight(18),
                          ),
                          AppTextFieldWidget(
                            hint: 'Phone',
                            controller: _phoneController,
                            textInputType: TextInputType.phone,
                            errorMsg: _phoneError,
                            hintSize: SizeConfig.scaleTextFont(14),
                            textSize: SizeConfig.scaleTextFont(14),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              AppButtonWidget(() => performEdit(), 'Save')
            ],
          ),
        ),
      ),
    );
  }

  void performEdit() {
    if (checkData()) {
      edit();
    }
  }

  bool checkData() {
    if (_firstNameController.text.isNotEmpty &&
        _lastNameController.text.isNotEmpty &&
        _phoneController.text.isNotEmpty &&
        _emailController.text.isNotEmpty) {
      checkErrors();
      return true;
    }

    checkErrors();
    Helper.showSnackBar(context,
        text: 'Please, enter required data', error: true);
    return false;
  }

  void edit() {
    Navigator.pushReplacementNamed(context, '/settings_screen');
  }

  void checkErrors() {
    setState(() {
      _firstNameError =
          _firstNameController.text.isEmpty ? 'Enter First Name' : null;
      _phoneError = _phoneController.text.isEmpty ? 'Enter Phone Number' : null;
      _emailError = _emailController.text.isEmpty ? 'Enter Email' : null;
      _lastNameError =
          _lastNameController.text.isEmpty ? 'Enter Last Name' : null;
    });
  }
}
