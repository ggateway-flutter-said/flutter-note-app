import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:notes/helpers/helper.dart';
import 'package:notes/screens/categories_screen.dart';
import 'package:notes/utils/size_config.dart';
import 'package:notes/utils/styles.dart';
import 'package:notes/widgets/app_textfield_widget.dart';
import 'package:notes/widgets/app_button_widget.dart';
import 'package:notes/widgets/app_text_widget.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  late TextEditingController _firstNameController;
  late TextEditingController _lastNameController;
  late TextEditingController _emailController;
  late TextEditingController _passwordController;
  late TextEditingController _phoneController;
  String? _firstNameError;
  String? _lastNameError;
  String? _emailError;
  String? _passwordError;
  String? _phoneError;

  @override
  void initState() {
    super.initState();
    _firstNameController = TextEditingController(text: 'Kendrick');
    _lastNameController = TextEditingController();
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
    _phoneController = TextEditingController();
  }

  @override
  void dispose() {
    _firstNameController.dispose();
    _lastNameController.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _phoneController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/app_background.png'),
          fit: BoxFit.cover,
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsetsDirectional.only(
              top: SizeConfig.scaleHeight(53),
              start: SizeConfig.scaleWidth(25),
              end: SizeConfig.scaleWidth(25),
            ),
            child: Column(
              children: [
                AppTextWidget(
                  text: 'Sign In',
                  fontSize: SizeConfig.scaleTextFont(30),
                  fontFamily: 'nunito',
                ),
                AppTextWidget(
                  text: 'Create an account',
                  fontWeight: FontWeight.w300,
                  color: AppStyleColor.TEXT_COLOR_GRAY,
                  fontSize: SizeConfig.scaleTextFont(18),
                ),
                Card(
                  margin: EdgeInsetsDirectional.only(
                    top: SizeConfig.scaleHeight(81),
                    bottom: SizeConfig.scaleHeight(30),
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: SizeConfig.scaleWidth(15),
                      vertical: SizeConfig.scaleHeight(32),
                    ),
                    child: Column(
                      children: [
                        AppTextFieldWidget(
                          hint: 'FirstName',
                          controller: _firstNameController,
                          errorMsg: _firstNameError,
                          hintSize: SizeConfig.scaleTextFont(22),
                          textSize: SizeConfig.scaleTextFont(22),
                        ),
                        SizedBox(
                          height: SizeConfig.scaleHeight(18),
                        ),
                        AppTextFieldWidget(
                          hint: 'LastName',
                          controller: _lastNameController,
                          errorMsg: _lastNameError,
                          hintSize: SizeConfig.scaleTextFont(22),
                          textSize: SizeConfig.scaleTextFont(22),
                        ),
                        SizedBox(
                          height: SizeConfig.scaleHeight(18),
                        ),
                        AppTextFieldWidget(
                          hint: 'Email',
                          controller: _emailController,
                          textInputType: TextInputType.emailAddress,
                          errorMsg: _emailError,
                          hintSize: SizeConfig.scaleTextFont(22),
                          textSize: SizeConfig.scaleTextFont(22),
                        ),
                        SizedBox(
                          height: SizeConfig.scaleHeight(18),
                        ),
                        AppTextFieldWidget(
                          hint: 'Phone',
                          controller: _phoneController,
                          textInputType: TextInputType.phone,
                          errorMsg: _phoneError,
                          hintSize: SizeConfig.scaleTextFont(22),
                          textSize: SizeConfig.scaleTextFont(22),
                        ),
                        SizedBox(
                          height: SizeConfig.scaleHeight(18),
                        ),
                        AppTextFieldWidget(
                          hint: 'Password',
                          controller: _passwordController,
                          isPassword: true,
                          errorMsg: _passwordError,
                          hintSize: SizeConfig.scaleTextFont(22),
                          textSize: SizeConfig.scaleTextFont(22),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: SizeConfig.scaleHeight(30),
                ),
                AppButtonWidget(
                  () {
                    performSignup();
                  },
                  'Sign up',
                ),
              ],
            ),
          ),
        ),
      ),
      // body: ,
    );
  }

  void performSignup() {
    if (checkData()) {
      signup();
    }
  }

  bool checkData() {
    if (_firstNameController.text.isNotEmpty &&
        _lastNameController.text.isNotEmpty &&
        _phoneController.text.isNotEmpty &&
        _emailController.text.isNotEmpty &&
        _passwordController.text.isNotEmpty) {
      checkErrors();
      return true;
    }

    checkErrors();
    Helper.showSnackBar(context,
        text: 'Please, enter required data', error: true);
    return false;
  }

  void signup() {
    Future.delayed(Duration(seconds: 3), () {
      Navigator.pushReplacementNamed(context, '/category_screen');
    });
  }

  void checkErrors() {
    setState(() {
      _firstNameError =
          _firstNameController.text.isEmpty ? 'Enter First Name' : null;
      _passwordError =
          _passwordController.text.isEmpty ? 'Enter Password' : null;
      _phoneError = _phoneController.text.isEmpty ? 'Enter Phone Number' : null;
      _emailError = _emailController.text.isEmpty ? 'Enter Email' : null;
      _lastNameError =
          _lastNameController.text.isEmpty ? 'Enter Last Name' : null;
    });
  }
}
