import 'package:flutter/material.dart';
import 'package:notes/helpers/helper.dart';
import 'package:notes/utils/size_config.dart';
import 'package:notes/utils/styles.dart';
import 'package:notes/widgets/app_textfield_widget.dart';
import 'package:notes/widgets/app_button_widget.dart';
import 'package:notes/widgets/app_text_widget.dart';

class AddCategoryScreen extends StatefulWidget {
  @override
  _AddCategoryScreenState createState() => _AddCategoryScreenState();
}

class _AddCategoryScreenState extends State<AddCategoryScreen> {
  late TextEditingController _nameCategoryController;
  late TextEditingController _descriptionController;
  String? errorNameCategory;
  String? errorDescription;

  @override
  void initState() {
    super.initState();
    _nameCategoryController = TextEditingController();
    _descriptionController = TextEditingController();
  }

  @override
  void dispose() {
    _nameCategoryController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsetsDirectional.only(
            top: SizeConfig.scaleHeight(15),
            start: SizeConfig.scaleWidth(25),
            end: SizeConfig.scaleWidth(25),
          ),
          child: Column(
            children: [
              AppTextWidget(
                text: 'New Category',
                fontSize: SizeConfig.scaleTextFont(30),
                fontFamily: 'nunito',
              ),
              AppTextWidget(
                text: 'Create category',
                fontWeight: FontWeight.w300,
                color: AppStyleColor.TEXT_COLOR_GRAY,
                fontSize: SizeConfig.scaleTextFont(18),
              ),
              Card(
                margin: EdgeInsetsDirectional.only(
                    top: SizeConfig.scaleHeight(81),
                    bottom: SizeConfig.scaleHeight(30)),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: SizeConfig.scaleWidth(20),
                      vertical: SizeConfig.scaleHeight(30)),
                  child: Column(
                    children: [
                      AppTextFieldWidget(
                        hintSize: SizeConfig.scaleTextFont(22),
                        textSize: SizeConfig.scaleTextFont(22),
                        controller: _nameCategoryController,
                        hint: 'Category Title',
                        errorMsg: errorNameCategory,
                      ),
                      SizedBox(
                        height: SizeConfig.scaleHeight(30),
                      ),
                      AppTextFieldWidget(
                        hintSize: SizeConfig.scaleTextFont(22),
                        textSize: SizeConfig.scaleTextFont(22),
                        controller: _descriptionController,
                        hint: 'Category Description',
                        errorMsg: errorDescription,
                      ),
                    ],
                  ),
                ),
              ),
              AppButtonWidget(
                () => performData(),
                'Save',
              ),
            ],
          ),
        ),
      ),
    );
  }

  void performData() {
    if (checkData()) {
      addCategory();
    }
  }

  bool checkData() {
    if (_nameCategoryController.text.isNotEmpty &&
        _descriptionController.text.isNotEmpty) {
      checkErrors();
      return true;
    }
    checkErrors();
    Helper.showSnackBar(context,
        text: 'Please, enter required data', error: true);
    return false;
  }

  void addCategory() {
    Navigator.pushReplacementNamed(context, '/category_screen');
  }

  void checkErrors() {
    setState(() {
      errorNameCategory =
          _nameCategoryController.text.isEmpty ? 'Enter Category Name' : null;
      errorDescription =
          _descriptionController.text.isEmpty ? 'Enter Category Description' : null;
    });
  }
}
